import pandas as pd

data = pd.ExcelFile('CITAM Budget Template5.xlsm')

# Now define a function for doing the reshape


def ReshapeFunc(excel_obj, i):
    """ Takes in an excel file object with multiple tabs in a wide format, 
    and a specified index of the tab to be parsed and reshaped. 
    Returns a data frame of the specified tab reshaped to long format"""

    tabnames = data.sheet_names
    assert i < len(
        tabnames), "Your tab index exceeds the number of available tabs, try a lower number"

    # parse and clean columns
    df = excel_obj.parse(sheet_name=tabnames[i], skiprows=5)
    cols1 = [str(x)[:4] for x in list(df.columns)]
    cols2 = [str(x) for x in list(df.iloc[0, :])]
    cols = [x+"_"+y for x, y in zip(cols1, cols2)]
    df.columns = cols
    df = df.drop(["Unna_nan"], axis=1).iloc[1:, :].rename(columns={
        'Unna_Revenue': 'Revenue'})
    # new columns, drop some and change data type
    df['main_func'] = tabnames[3].split(
        " - ")[0] + " " + tabnames[3].split(" - ")[1]
#     df.drop([c for c in df.columns if "Total" in c], axis=1, inplace= True)

#     for c in [c for c in df.columns if "yrs" in c]:
#         df[c] = df[c].apply(lambda x: pd.to_numeric(x))
    # reshape - indexing, pivoting and stacking
    idx = ['Revenue']
    multi_indexed_df = df.set_index(idx)
    stacked_df = multi_indexed_df.stack(dropna=False)
    long_df = stacked_df.reset_index()

    # clean up and finalize
    col_str = long_df.level_1.str.split("_")
    long_df['fin_period'] = [x[0] for x in col_str]
    long_df['budg_type'] = [x[1] for x in col_str]
    long_df['target_quantity'] = long_df[0]  # rename this column
    df_final = long_df.drop(['level_1', 0], axis=1)
    return df_final


# start_time = timeit.default_timer()
#use the function to make a data frame for each tab, put them all in a list, concatenate them into one single data frame and save it back to Excel.
dfs_list = [ReshapeFunc(data, i) for i in range(3, 14)]
concat_dfs = pd.concat(dfs_list)
concat_dfs.to_excel("Reshaped_CITAM_ResultD.xlsx")

# elapsed = timeit.default_timer() - start_time
# print(elapsed)
