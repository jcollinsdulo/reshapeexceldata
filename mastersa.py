import os
from multiprocessing import Pool
import run_processes



def test():

    processes = (r'C:\Users\JobCollins\reshapeexceldata\processa.py', 
                    r'C:\Users\JobCollins\reshapeexceldata\processb.py',
                    r'C:\Users\JobCollins\reshapeexceldata\processc.py', 
                    r'C:\Users\JobCollins\reshapeexceldata\processd.py', 
                    r'C:\Users\JobCollins\reshapeexceldata\processe.py')
    print("Creating 5 (non-daemon) workers and jobs in main process.")
    pool = Pool(processes=5)

    pool.map(run_processes.run_process, processes)


if __name__ == '__main__':
    test()
