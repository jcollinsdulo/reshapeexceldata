import os
# a tuple with the name of all scripts that we wish to run in parallel.
processes = ('processa.py', 'processb.py', 'processc.py', 'processd.py', 'processe.py')


def run_process(process):
        # a function to execute a system command.
        # In this case we will use the standard library os and the method .system,
        # that allows you to run commands to your operational system just as if you are in any terminal.
        os.system('python {}'.format(process))
